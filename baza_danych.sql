create database animal_friendly_places;
use animal_friendly_places;
drop database animal_friendly_places;

create table places
(
    id     int auto_increment primary key,
    name   varchar(100),
    type   varchar(100),
    adress varchar(100),
    phone        char(9)

);


insert into places(name, type, adress, phone)
values ('Kot Cafe', 'Kawiarnia', 'Wrocław, Stanisława Dubois 25', '456123569'),
('Hotel Sułkowski', 'Hotel' , 'Boszkowo, Dworcowa 15', '655371100'),
('Leroy Merlin', 'Sklep' , 'Wrocław, Krakowska 51', '652678231'),
('Zalew Miętkowski', 'Kąpielisko' , 'Miętków', '456123402'),
('VEGA', 'Restauracja' , 'Wrocław, Rynek 27a', '784274098'),
('Broawr pod złotym psem', 'Restauracja' , 'Wrocław, Rynek 41', '456234876'),
('Park Jana Pawła II', 'Park' , 'Poznań', '783987458'),
('Park Przy Dolinie Marzeń', 'Park' , 'Toruń', '765983028'),
('Bułka z masłem', 'Restauracja' , 'Wrocław, plac Solny 14', '123567346'),
('Pałac Radziejowice', 'Park' , 'Radziejowice, ul. Sienkiewicza 4', '758395820'),
('Szafoniera', 'Restauracja' , 'Puszczykowo, ul. Poznańska 117A', '476243829'),
('Zalew Kodrąb', 'Kąpielisko' , 'Kodrąb', '673956203');


DROP USER 'admin2417'@'localhost';
CREATE USER 'admin2417'@'localhost' IDENTIFIED BY 'tajnehaslo123';
GRANT ALL PRIVILEGES ON animal_friendly_places.places TO 'admin2417'@'localhost';
