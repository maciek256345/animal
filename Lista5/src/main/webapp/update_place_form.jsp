<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Update Place</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<br>
<h1 style="text-align: center; font-size: 30px">Wprowadź zaktualizowane dane</h1>
<form action="AdminServlet" method="get">
    <input type="hidden" name="command" value="UPDATE"/>
    <input type="hidden" name="placeID" value="${PLACE.id}"/>

    <div class="container"
         style="margin-top: 40px; background-color: whitesmoke; padding-top: 25px; padding-bottom: 25px; width: 40%; border-radius: 10px">

        <form>

            <div class="mb-3">
                <label for="name" class="form-label" style="font-size: 20px">Nazwa miejsca:</label>
                <input type="text" class="form-control" id="name" name="nameInput" value="${PLACE.name}">
            </div>
            <br>
            <div class="mb-3">
                <label for="adress" class="form-label" style="font-size: 20px">Adres miejsca:</label>
                <input type="text" class="form-control" id="adress" name="adressInput" value="${PLACE.adress}">
            </div>
            <br>
            <div class="mb-3">
                <label for="phone" class="form-label" style="font-size: 20px">Numer telefonu:</label>
                <input type="tel" class="form-control" id="phone" name="phoneInput" value="${PLACE.phone}">
            </div>
            <br>
            <fieldset class="form-group" name="placeType">
                <legend class="mb-3" style="font-size: 20px">Typ miejsca:</legend>
                <fieldset class="form-group">
                    <label>
                        <select class="form-control" style="width: 250px " name="inlineRadioOptions">
                            <option value="${PLACE.type}">Wybierz typ</option>
                            <option value="Hotel" name="type3">Hotel</option>
                            <option value="Kawiarnia" name="type3">Kawiarnia</option>
                            <option value="Sklep">Sklep</option>
                            <option value="Kąpielisko">Kąpielisko</option>
                            <option value="Fryzjer">Fryzjer</option>
                            <option value="Apteka">Apteka</option>
                            <option value="Restauracja">Restauracja</option>
                            <option value="Park">Park</option>
                            <option value="Urząd">Urząd</option>
                        </select>
                    </label>
                </fieldset>
            </fieldset>

            <br>


            <div style="text-align: center">
                <button type="submit" class="btn btn-primary" style="background-color: 	#CD853F;
             border-color: 	#CD853F;
            padding-inline: 20px">Aktualizuj dane
                </button>
            </div>

        </form>

    </div>
</form>
<img src="animals2.png" class="rounded mx-auto d-block" alt="image3" style="width: 900px">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>
