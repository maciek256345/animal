<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,com.example.animalfriendly.AnimalFriendlyPlace" %>
<html>
<head>
    <title>Places</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/main.css">

<body>
<br>
<h1 style="text-align: center; font-size: 30px">Lista miejsc przyjaznych zwierzętom</h1>
<br>
<br>
<table class="table table-striped">

    <thead class="thead-dark">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Miejsce</th>
        <th scope="col">Typ</th>
        <th scope="col">Adres</th>
        <th scope="col">Numer telefonu</th>
        <th scope="col">Akcje</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="tmpPlace" items="${PLACES_LIST}">

        <c:url var="updateLink" value="AdminServlet">
            <c:param name="command" value="LOAD"></c:param>
            <c:param name="placeID" value="${tmpPlace.id}"></c:param>
        </c:url>

        <c:url var="deleteLink" value="AdminServlet">
            <c:param name="command" value="DELETE"></c:param>
            <c:param name="placeID" value="${tmpPlace.id}"></c:param>
        </c:url>

        <tr>
            <th scope="row">${tmpPlace.id}</th>
            <td>${tmpPlace.name}</td>
            <td>${tmpPlace.type}</td>
            <td>${tmpPlace.adress}</td>
            <td>${tmpPlace.phone}</td>
            <td><a href="${updateLink}">
                <button type="button" class="btn btn-success">Aktualizuj dane</button>
            </a>
                <a href="${deleteLink}"
                   onclick="if(!(confirm('Czy na pewno chcesz usunąć miejsce ?'))) return false">
                    <button type="button" class="btn btn-danger">Usuń miejsce</button>
                </a></td>
        </tr>

    </c:forEach>
    </tbody>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</table>
<br>
<br>
<br>

<div class="container" style="text-align: center; width: 50%">
    <div class="row">
        <div class="col-md-6">
            <p><a class="btn btn-primary btn-info" href="add_place_form.jsp" role="button"
                  style="background-color:  DimGray;  padding: 15px; font-weight: bold">Dodaj miejsce przyjazne
                zwierzętom</a></p>
        </div>
        <div class="col-md-6">
            <p><a class="btn btn-primary btn-info" href="index.html" role="button"
                  style="background-color:  DimGray;  padding: 15px; font-weight: bold">Wróć do strony głównej</a></p>
        </div>
    </div>
</div>

</body>
</html>

