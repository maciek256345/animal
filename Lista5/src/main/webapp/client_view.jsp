<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,com.example.animalfriendly.AnimalFriendlyPlace" %>
<html>
<head>
    <title>Places</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/main.css">

<body>
<br>
<h1 style="text-align: center; font-size: 34px; font-weight: bold">Lista miejsc przyjaznych zwierzętom</h1>

<br>

<form class="form"
      style="background-color: whitesmoke; width: 30%; margin-left: 50px; padding-top: 15px; border-radius: 10px"
      action="ClientServlet"
      method="get">
    <input type="hidden" name="command" value="SHOW">
    <div class="col">
        <label style="padding-left: 10px; padding-bottom: 10px; font-weight: bold; font-size: 18px"> Wyszukaj obiekty
            danego typu</label>
    </div>
    <div class="row">
        <div class="form-group">
            <fieldset class="form-group" style="padding-inline: 40px">
                <label>
                    <select class="form-control" style="width: 250px " name="types">
                        <option value="Pokaż wszystkie">Wybierz typ</option>
                        <option value="Hotel" name="type3">Hotele</option>
                        <option value="Kawiarnia" name="type3">Kawiarnie</option>
                        <option value="Sklep">Sklepy</option>
                        <option value="Kąpielisko">Kąpieliska</option>
                        <option value="Fryzjer">Fryzjerzy</option>
                        <option value="Apteka">Apteki</option>
                        <option value="Restauracja">Restauracje</option>
                        <option value="Park">Parki</option>
                        <option value="Urząd">Urzędy</option>
                    </select>
                </label>
            </fieldset>
        </div>
        <div class="form-group" style="padding-inline: 40px">
            <button type="submit" class="btn btn-primary" style="background-color: 	DimGray;
             border-color: 	DimGray;">Wyszukaj
            </button>
        </div>
    </div>
</form>
<br>
<br>
<table class="table table-striped">

    <thead class="thead-dark">
    <tr>
        <th scope="col">Miejsce</th>
        <th scope="col">Typ</th>
        <th scope="col">Adres</th>
        <th scope="col">Numer telefonu</th>

    </tr>
    </thead>
    <tbody>

    <c:forEach var="tmpPlace" items="${PLACES_LIST}">

        <tr>
            <th scope="row">${tmpPlace.name}</th>
            <td>${tmpPlace.type}</td>
            <td>${tmpPlace.adress}</td>
            <td>${tmpPlace.phone}</td>

        </tr>

    </c:forEach>
    </tbody>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</table>
<br>
<br>
<br>
<br>
<div class="container" style="text-align: center">
    <div class="col">
        <p><a class="btn btn-primary btn-info" href="index.html" role="button"
              style="background-color:  DimGray;  padding: 15px; font-weight: bold">Wróć do strony głównej</a></p>
    </div>

</div>


</body>
</html>
