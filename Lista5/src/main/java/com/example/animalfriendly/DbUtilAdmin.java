package com.example.animalfriendly;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DbUtilAdmin class extends DBUtil class.
 * This class contains methods that are
 * intended ony for admins.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
public class DbUtilAdmin extends DBUtil {

    private String URL;
    private String name;
    private String password;

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public DbUtilAdmin(String URL) {
        this.URL = URL;
    }

    /**
     * This method allows to get
     * all places from database
     * with their ID.
     *
     * @return palces - a list of places.
     */
    @Override
    List<AnimalFriendlyPlace> getPlaces() throws Exception {

        List<AnimalFriendlyPlace> places = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = DriverManager.getConnection(URL, name, password);

            statement = conn.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM places");

            while (resultSet.next()) {

                places.add(new AnimalFriendlyPlace(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("adress"),
                        resultSet.getString("phone")
                ));
            }
        } finally {
            close(conn, statement, resultSet);
        }

        return places;
    }

    /**
     * This method allows to add
     * a new object to database
     *
     * @param place - place which is going to be added
     * @throws Exception
     */
    public void addPlace(AnimalFriendlyPlace place) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = DriverManager.getConnection(URL, name, password);
            String sql = "INSERT INTO places(name, type, adress, phone) " +
                    "VALUES(?,?,?,?)";

            statement = conn.prepareStatement(sql);
            statement.setString(1, place.getName());
            statement.setString(2, place.getType());
            statement.setString(3, place.getAdress());
            statement.setString(4, place.getPhone());

            statement.execute();

        } finally {
            close(conn, statement, null);
        }
    }

    /**
     * This method allows to get
     * obly one place from database.
     *
     * @param id - ID of place which user want to get know
     * @return place - place with given ID
     */
    public AnimalFriendlyPlace getPlace(String id) throws Exception {

        AnimalFriendlyPlace place = null;
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            int placeID = Integer.parseInt(id);
            conn = DriverManager.getConnection(URL, name, password);
            statement = conn.prepareStatement("SELECT * FROM places WHERE id=?");
            statement.setInt(1, placeID);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {

                place = new AnimalFriendlyPlace(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("adress"),
                        resultSet.getString("phone")
                );

            } else {
                throw new Exception("Nie ma takiego id " + placeID);
            }
            return place;

        } finally {
            close(conn, statement, resultSet);
        }
    }

    /**
     * This method allows to update place
     * in database.
     *
     * @param place - a place to be updated
     */
    public void updatePlace(AnimalFriendlyPlace place) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = DriverManager.getConnection(URL, name, password);

            String sql = "UPDATE places SET name=?, type=?, adress=?, phone=? WHERE id=?";

            statement = conn.prepareStatement(sql);
            statement.setString(1, place.getName());
            statement.setString(2, place.getType());
            statement.setString(3, place.getAdress());
            statement.setString(4, place.getPhone());
            statement.setInt(5, place.getId());
            statement.execute();

        } finally {
            close(conn, statement, null);
        }
    }

    /**
     * This method allows to delete
     * specified place from database
     *
     * @param id - ID of place to be deleted
     */
    public void deletePlace(String id) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {

            int placeID = Integer.parseInt(id);

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "DELETE FROM places WHERE id=?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, placeID);

            statement.execute();

        } finally {
            close(conn, statement, null);
        }
    }
}
