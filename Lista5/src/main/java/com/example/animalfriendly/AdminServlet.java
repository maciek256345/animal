package com.example.animalfriendly;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

/**
 * This class extends HttpServlet.
 * AdminServlet class is responsible
 * for processing admin http requests.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
@WebServlet(name = "AdminServlet", value = "/AdminServlet")
public class AdminServlet extends HttpServlet {
    private DbUtilAdmin dbUtil;
    private final String url = "jdbc:mysql://localhost:3306/animal_friendly_places?useSSL=false&allowPublicKeyRetrieval=true&" +
            "serverTimezone=CET";

    /**
     * This method is called
     * during initialization and is
     * responsible for creating an object
     * of class DbUtilAdmin.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {
            dbUtil = new DbUtilAdmin(url);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * This method is responsible for
     * handling "GET" requests. Different
     * methods are called depending on the
     * specific request.
     *
     * @param request  - client request
     * @param response - server resposne
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String command = request.getParameter("command");

            if (command == null) {
                command = "LIST";
            }

            switch (command) {
                case "LIST":
                    listPlaces(request, response);
                    break;
                case "ADD":
                    addPlace(request, response);
                    break;
                case "LOAD":
                    loadPlace(request, response);
                    break;
                case "UPDATE":
                    updatePlace(request, response);
                    break;
                case "DELETE":
                    deletePlace(request, response);
                    break;
                default:
                    listPlaces(request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * This method is responsible for
     * handling "POST" requests.
     * Additionally, the correctness of
     * the administrator's login and
     * password is checked
     *
     * @param request  - client request
     * @param response - server resposne
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        String name = request.getParameter("loginInput");
        String password = request.getParameter("passwordInput");

        dbUtil.setName(name);
        dbUtil.setPassword(password);

        if (validate(name, password)) {

            RequestDispatcher dispatcher = request.getRequestDispatcher
                    ("/admin_view.jsp");
            List<AnimalFriendlyPlace> placesList = null;

            try {
                placesList = dbUtil.getPlaces();
            } catch (Exception e) {
                e.printStackTrace();
            }

            request.setAttribute("PLACES_LIST", placesList);
            dispatcher.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/index.html");
            dispatcher.include(request, response);
        }
    }

    /**
     * The method responsible for
     * determining whether there
     * is a user with a given login
     * and password.
     *
     * @param name - admin login
     * @param pass - admin password
     * @return status - boolean value about access
     */
    private boolean validate(String name, String pass) {

        boolean status = false;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url, name, pass);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    /**
     * This method is responsible for
     * getting all places from database and
     * sending them to admin_view.jsp file.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void listPlaces(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        List<AnimalFriendlyPlace> placeList = dbUtil.getPlaces();
        request.setAttribute("PLACES_LIST", placeList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * This method is responsible for
     * adding a new place with the
     * entered parameters to database.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void addPlace(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String name = request.getParameter("nameInput");
        String type = request.getParameter("inlineRadioOptions");
        String adress = request.getParameter("adressInput");
        String phone = request.getParameter("phoneInput");

        AnimalFriendlyPlace place = new AnimalFriendlyPlace(name, type, adress, phone);
        dbUtil.addPlace(place);
        listPlaces(request, response);
    }

    /**
     * This method is responsible for
     * getting one place with specified
     * ID from database.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void loadPlace(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String id = request.getParameter("placeID");
        AnimalFriendlyPlace place = dbUtil.getPlace(id);

        request.setAttribute("PLACE", place);

        RequestDispatcher dispatcher = request.getRequestDispatcher
                ("/update_place_form.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * This method is responsible
     * for updating selected place
     * in database.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void updatePlace(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int id = Integer.parseInt(request.getParameter("placeID"));
        String name = request.getParameter("nameInput");
        String type = request.getParameter("inlineRadioOptions");
        String adress = request.getParameter("adressInput");
        String phone = request.getParameter("phoneInput");

        AnimalFriendlyPlace place = new AnimalFriendlyPlace(id, name, type, adress, phone);
        dbUtil.updatePlace(place);
        listPlaces(request, response);
    }

    /**
     * This method is responsible
     * for deleting selected place
     * from database.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void deletePlace(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String id = request.getParameter("placeID");
        dbUtil.deletePlace(id);
        listPlaces(request, response);

    }

}
    