package com.example.animalfriendly;

/**
 * This class represents animal friendly place
 * from database. One constructor is with place ID
 * (for admins) and second is without.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
public class AnimalFriendlyPlace {
    private int id;
    private String name;
    private String type;
    private String adress;
    private String phone;

    public AnimalFriendlyPlace(int id, String name, String type, String adress, String phone) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.adress = adress;
        this.phone = phone;
    }

    public AnimalFriendlyPlace(String name, String type, String adress, String phone) {
        this.name = name;
        this.type = type;
        this.adress = adress;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
