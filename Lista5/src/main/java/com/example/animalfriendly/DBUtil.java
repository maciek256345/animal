package com.example.animalfriendly;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * DBUtil is abstract class.
 * This class has a method that allows to
 * close Connection, Statement and ResultSet.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
public abstract class DBUtil {

    abstract List<AnimalFriendlyPlace> getPlaces() throws Exception;

    /**
     * This method is resposnible for closing
     * Connection, Statement and ResultSet
     *
     * @param conn      - Connection
     * @param statement - Statement
     * @param resultSet - ResulSet
     */
    protected static void close(Connection conn, Statement statement,
                                ResultSet resultSet) {

        try {
            if (resultSet != null)
                resultSet.close();

            if (statement != null)
                statement.close();

            if (conn != null)
                conn.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
