package com.example.animalfriendly;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * DbUtilClient class extends DBUtil class.
 * This class contains methods
 * which are intended for common user.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
public class DbUtilClient extends DBUtil {

    private DataSource dataSource;

    public DbUtilClient(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * This method allows to get all places
     * from database
     *
     * @return places - a list of animal friendly places
     */
    @Override
    List<AnimalFriendlyPlace> getPlaces() throws Exception {

        List<AnimalFriendlyPlace> places = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();
            statement = conn.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM places");

            while (resultSet.next()) {

                places.add(new AnimalFriendlyPlace(
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("adress"),
                        resultSet.getString("phone")

                ));

            }

        } finally {
            close(conn, statement, resultSet);
        }

        return places;
    }

    /**
     * This method allows to
     * get places with specified
     * type from database
     *
     * @param placeType - type of place wich user want to see
     * @return places - a list of places of a given type
     */
    List<AnimalFriendlyPlace> getSelectedTypePlaces(String placeType) throws Exception {

        List<AnimalFriendlyPlace> places = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();
            statement = conn.createStatement();
            String sql = null;

            if (placeType.equals("Pokaż wszystkie")) {
                sql = "SELECT * FROM places";
            } else {
                sql = "SELECT * FROM places WHERE type LIKE '" + placeType + "'";
            }
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                places.add(new AnimalFriendlyPlace(
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("adress"),
                        resultSet.getString("phone")

                ));
            }

        } finally {
            close(conn, statement, resultSet);
        }

        return places;
    }
}
