package com.example.animalfriendly;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

/**
 * This class extends HttpServlet.
 * ClientServlet class is responsible
 * for processing http requests.
 *
 * @author Maciej Suliński
 * @version 1.0
 * @since 2022-04-14
 */
@WebServlet(name = "ClientServlet", value = "/ClientServlet")
public class ClientServlet extends HttpServlet {

    private DataSource dataSource;
    private DbUtilClient dbUtil;

    public ClientServlet() throws NamingException {

        Context initCtx = new InitialContext();
        Context envCtx = (Context) initCtx.lookup("java:comp/env");
        dataSource = (DataSource) envCtx.lookup("jdbc/places_web_app");
    }

    /**
     * This method is called
     * during initialization and is
     * responsible for creating an object
     * of class DbUtilClient.
     */
    @Override
    public void init() throws ServletException {
        super.init();

        try {
            dbUtil = new DbUtilClient(dataSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is responsible for
     * handling "GET" requests.
     *
     * @param request  - client request
     * @param response - server resposne
     */
    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        try {

            String command = request.getParameter("command");

            if (command == null)
                command = "LIST";

            switch (command) {
                case "LIST":
                    listPlaces(request, response);
                    break;
                case "SHOW":
                    listSelectedTypePlaces(request, response);
                    break;
                default:
                    listPlaces(request, response);
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }


    /**
     * This method is responsible for
     * handling "POST" requests.
     *
     * @param request  - client request
     * @param response - server resposne
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    /**
     * This method is responsible for
     * getting places of selected type
     * from database and sending them
     * to client_view.jsp file.
     *
     * @param request  - client request
     * @param response - server response
     */
    public void listSelectedTypePlaces(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String type = request.getParameter("types");
        System.out.println(type);

        dbUtil.getSelectedTypePlaces(type);

        List<AnimalFriendlyPlace> placesList = dbUtil.getSelectedTypePlaces(type);
        request.setAttribute("PLACES_LIST", placesList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/client_view.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * This method is responsible for
     * getting all places from database and
     * sending them to client_view.jsp file.
     *
     * @param request  - client request
     * @param response - server response
     */
    private void listPlaces(HttpServletRequest request, HttpServletResponse
            response) throws Exception {

        List<AnimalFriendlyPlace> placesList = dbUtil.getPlaces();
        request.setAttribute("PLACES_LIST", placesList);

        RequestDispatcher dispatcher = request.getRequestDispatcher(
                "/client_view.jsp");
        dispatcher.forward(request, response);
    }
}
