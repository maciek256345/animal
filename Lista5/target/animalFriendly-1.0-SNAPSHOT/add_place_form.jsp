<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Place</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<br>
<h1 style="font-size: 30px; text-align: center">
    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-house-fill"
         viewBox="0 0 16 16">
        <path fill-rule="evenodd"
              d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"></path>
        <path fill-rule="evenodd"
              d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"></path>
    </svg>
    Dodaj miejsce przyjazne zwierzętom
</h1>
<br>
<form action="AdminServlet" method="get">
    <input type="hidden" name="command" value="ADD">
    <div class="container"
         style="margin-top: 40px; background-color: whitesmoke; padding-top: 25px; padding-bottom: 25px; width: 40%; border-radius: 10px">
        <form>
            <div class="mb-3">
                <label for="name" class="form-label" style="font-size: 20px">Nazwa miejsca:</label>
                <input type="text" class="form-control" id="name" name="nameInput">
            </div>
            <br>
            <div class="mb-3">
                <label for="adress" class="form-label" style="font-size: 20px">Adres miejsca:</label>
                <input type="text" class="form-control" id="adress" name="adressInput">
            </div>
            <br>
            <div class="mb-3">
                <label for="phone" class="form-label" style="font-size: 20px">Numer telefonu:</label>
                <input type="tel" class="form-control" id="phone" name="phoneInput">
            </div>
            <br>
            <fieldset class="form-group" name="placeType">
                <legend class="mb-3" style="font-size: 20px">Typ miejsca:</legend>
                <fieldset class="form-group">
                    <label>
                        <select class="form-control" style="width: 250px " name="inlineRadioOptions">
                            <option value="Pokaż wszystkie">Wybierz typ</option>
                            <option value="Hotel" name="type3">Hotel</option>
                            <option value="Kawiarnia" name="type3">Kawiarnia</option>
                            <option value="Sklep">Sklep</option>
                            <option value="Kąpielisko">Kąpielisko</option>
                            <option value="Fryzjer">Fryzjer</option>
                            <option value="Apteka">Apteka</option>
                            <option value="Restauracja">Restauracja</option>
                            <option value="Park">Park</option>
                            <option value="Urząd">Urząd</option>
                        </select>
                    </label>
                </fieldset>
            </fieldset>
            <br>
            <div style="text-align: center">
                <button type="submit" class="btn btn-primary" style="background-color: 	#CD853F;
             border-color: 	#CD853F;
            padding-inline: 20px">Dodaj miejsce
                </button>
            </div>

        </form>
    </div>
</form>
<img src="animals2.png" class="rounded mx-auto d-block" alt="image3" style="width: 900px">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>
